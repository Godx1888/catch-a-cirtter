﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Catch_a_Cirtter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MaxCRITTERS = 20;
        Cirtter[] critterPool = new Cirtter[MaxCRITTERS]; //  A array of citter reference
        Score ourScore; // a blank address for a player's score
        const float SPAWN_DELAY = 1.5f;
        float timeUNtilNextSpawn = SPAWN_DELAY;
        int currentCritterIndex = 0;
        Button ourButton;
        bool playing = false;
        Timer gameTimer = null;
        const float GAME_LENGTH = 30f;
    
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            ourScore = new Score();

            ourScore.LoadContent(Content);


            ourButton = new Button();

            ourButton.LoadContent(Content);

            gameTimer = new Timer();

            gameTimer.LoadContent(Content);
            gameTimer.SetTime(GAME_LENGTH);
            gameTimer.ourTimerCallBack += EndGame;
            // set the fuction that will be called when the button is clicked
            // (+= means it adds to any existing set there)
            ourButton.ourButtonCallBack += StartGame;



            //ourCritter = new Cirtter(ourScore);
            // call the critter's function to load its content
            // ourCritter.LoadContent(Content);


            // Call the spawn function

            //ourCritter.Spawn(Window);

            

            Random rand = new Random();
            // Create critter objects and load their content
            for(int i=0;  i<MaxCRITTERS; i++)
            {
                Cirtter.Species newSpecies = (Cirtter.Species)rand.Next(0, (int)Cirtter.Species.NUM);

                Cirtter newCirtter = new Cirtter(ourScore, newSpecies);
                newCirtter.LoadContent(Content);

                // add the newly created critter to our pool
                critterPool[i] = newCirtter;
            }




            IsMouseVisible = true;

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            //ourCritter.Input();

            if (playing == true)
            {
                gameTimer.Update(gameTime);

                foreach (Cirtter eachCritter in critterPool)
                {
                    // loops content
                    eachCritter.Input();
                }

                timeUNtilNextSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                // Should a new critter spawn
                if (timeUNtilNextSpawn <= 0f)
                {
                    timeUNtilNextSpawn = SPAWN_DELAY;
                    // spawn a new critter
                    // TODO: Make sure critter is not alive before spawning
                    // Spawn next critter in list
                    critterPool[currentCritterIndex].Spawn(Window);
                    ++currentCritterIndex;
                    if (currentCritterIndex >= critterPool.Length)
                    {
                        currentCritterIndex = 0;
                    }
                }

            }
            else
            {
                ourButton.Input();
            }
         
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            // began draw
            spriteBatch.Begin();

            ourScore.Draw(spriteBatch);
            ourButton.Draw(spriteBatch);
            gameTimer.Draw(spriteBatch);
            // draw here
            //ourCritter.Draw(spriteBatch);
            foreach (Cirtter eachCritter in critterPool)
            {
                // loops content
                eachCritter.Draw(spriteBatch);
            }

            

            // end draw
            spriteBatch.End();


            base.Draw(gameTime);
        }

        void StartGame()
        {
            playing = true;
            gameTimer.StartTimer();
            ourScore.resetScore();
        }
        void EndGame()
        {
            playing = false;
            // show button
            ourButton.Show();
            gameTimer.SetTime(GAME_LENGTH);
            
            // remove critters
            foreach (Cirtter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }
        }
    }
}
