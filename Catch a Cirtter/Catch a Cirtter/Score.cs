﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_a_Cirtter
{
    class Score
    {


        // ----------------------------
        // Data
        // ----------------------------
        int value = 0;
        Vector2 position = new Vector2(10, 10);
        SpriteFont font = null;

    

        // ----------------------------
        // Behaviour
        // ----------------------------

        public void AddScore(int toAdd)
        {
           // adds the provied number to our current score value
            value += toAdd;
        }

        // ----------------------------
        public void Draw(SpriteBatch spriteBatch)
        {

            // draw the score to the screnusing font variable
            spriteBatch.DrawString(font, "Score: " + value.ToString(), position, Color.White);
            

        }
        // ---------------------------

        public void LoadContent(ContentManager content)
        {

            font = content.Load<SpriteFont>("fonts/mainFont");

        }
        // ---------------------------
        public void resetScore()
        {
            value = 0;
        }
            

    }
}
