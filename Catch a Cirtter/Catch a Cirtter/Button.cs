﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Catch_a_Cirtter
{
    class Button
    {
        //--------------------------------
        // data
        //--------------------------------
        Texture2D image;
        Vector2 position = new Vector2(365,240);
        SoundEffect clickButton;
         bool visible = true;


        // delegates
      public delegate void onClick();
       public onClick ourButtonCallBack;

        //--------------------------------
        // Behaviour
        //--------------------------------

        public void LoadContent(ContentManager content)
        {
             image = content.Load<Texture2D>("graphics/button");
            
 
            clickButton = content.Load<SoundEffect>("audio/buttonclick");
        }
        // -------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if(visible == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
 
        }
        // ----------------------------------
        public void Input()
        {
            // get mouse state
            MouseState currentState = Mouse.GetState();
            // set rectangle
            Rectangle bounds = new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);
            // check if button has been click
            if (currentState.LeftButton == ButtonState.Pressed && bounds.Contains(currentState.X, currentState.Y) && visible == true)
            {
                // we click the button!
                clickButton.Play();

                // Despawn the Button
                visible = false;

                if(ourButtonCallBack != null)
                ourButtonCallBack();

            }


        }
        //------------------------------------
        public void Show()
        {
            visible = true;
        }

        //------------------------------------

    }
}
