﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_a_Cirtter
{
    class Timer
    {
        // ----------------------------
        // Data
        // ----------------------------
        float timeRemaining = 0;
        Vector2 position = new Vector2(200, 10);
        SpriteFont font = null;
        bool running = false;

        // delegates
        public delegate void TimeUp();
        public TimeUp ourTimerCallBack;

        // ----------------------------
        // Behaviour
        // ----------------------------

        public void LoadContent(ContentManager content)
        {

            font = content.Load<SpriteFont>("fonts/mainFont");

        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {

            // draw the time to the screnusing font variable
            int timeInt = (int)timeRemaining;
            spriteBatch.DrawString(font, "Time: " + timeInt.ToString(), position, Color.White);


        }
        // ---------------------------
        public void Update(GameTime gameTime)
        {
            if(running == true)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                // time runs out 
                if (timeRemaining <= 0)
                {
                    // stop
                    running = false;
                    timeRemaining = 0;

                    if(ourTimerCallBack != null)
                    {
                        ourTimerCallBack();
                    }
                }
            }
          
        }
        // ---------------------------
        public void StartTimer()
        {
            running = true;
        }
        // ---------------------------
        public void SetTime(float newTime)
        {
            timeRemaining = newTime;
        }
        // ---------------------------

    }
}
