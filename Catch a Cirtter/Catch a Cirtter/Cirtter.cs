﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Catch_a_Cirtter
{
    class Cirtter
    {
        // ----------------------------
        // definitions
        // ----------------------------
       public enum Species
            // a special type of integer with only specific values allowed, that have names
        {
            Bear,                      // 0
            Horse,                     // 1
            Elephant,                  // 2



            // --

            NUM          // 3
        }


        // ----------------------------
        // Data
        // ----------------------------
        Texture2D image;
        Vector2 position = Vector2.Zero;
        bool alive = false;
        Score scoreObject = null;
        int critterValue = 10;
        SoundEffect clickCritter;
        Species species = Species.Bear;
        

        // ----------------------------
        // Behaviour
        // ----------------------------
        public Cirtter( Score newScore, Species newSpecies)
        {
            // constructor called when the object is created
            // no reture type (special)
            // Helps decide how the object will be set up
            // can have arguments (such as new score)
            
            scoreObject = newScore;
            species = newSpecies;
        }

        // ----------------------------
        public void LoadContent(ContentManager content)
        {
    
           image = content.Load<Texture2D>("graphics/bear");


            switch (species)
            {
                case Species.Bear:
                    image = content.Load<Texture2D>("graphics/bear");
                    critterValue = 20;
                    break;

                case Species.Elephant:
                    image = content.Load<Texture2D>("graphics/elephant");
                    critterValue = 30;
                    break;

                case Species.Horse:
                    image = content.Load<Texture2D>("graphics/horse");
                    critterValue = 50;
                    break;

                default:
                    break;
            }




           clickCritter = content.Load<SoundEffect>("audio/buttonclick");

        }
        // ----------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
            
        }
        // ----------------------------
        public void Spawn(GameWindow window)
        {
            // set critter to be visable
            alive = true;


            // Set the bounds for the critter 
            int positionYmin = 0;
            int positionXmin = 0;
            int positionYMax = window.ClientBounds.Height - image.Height;
            int positionXMax = window.ClientBounds.Width - image.Width;
            
            // crates and set random spawn position
            Random rand = new Random();
            position.X = rand.Next(positionXmin, positionXMax);
            position.Y = rand.Next(positionYmin, positionYMax);
        }
        // ----------------------------
        public void Despawn()
        {
            // set critter to not visable to make it not draw and not clickable
            alive = false;
        }
        // ----------------------------
        public void Input()
        {
            // get mouse state
            MouseState currentState = Mouse.GetState();
            // set rectangle
            Rectangle critterBounds = new Rectangle((int)position.X, (int)position.Y, image.Width, image.Height);
            // check if critter has been click
            if (currentState.LeftButton == ButtonState.Pressed && critterBounds.Contains(currentState.X, currentState.Y)&& alive == true)
            {
                // we click the critter!
                clickCritter.Play();

                // Despawn the critter
                Despawn();


                // add to score
                scoreObject.AddScore(critterValue);

            }


        }
    }
}
